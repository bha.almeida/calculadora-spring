package br.com.calculadora.services;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.DTOs.RespostaDivisaoDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora){
        int resultado = 0;
        for(Integer numero : calculadora.getNumeros()){
            resultado = resultado + numero;
        }
        return new RespostaDTO(resultado);
    }

    public RespostaDTO subtrair(Calculadora calculadora){
        int resultado = calculadora.getNumeros().get(0);
        for(int i = 1; i < calculadora.getNumeros().size(); i++){
            resultado = resultado - calculadora.getNumeros().get(i);
        }
        return new RespostaDTO(resultado);
    }

    public RespostaDTO multiplicar(Calculadora calculadora){
        int resultado = calculadora.getNumeros().get(0);
        for(int i = 1; i < calculadora.getNumeros().size(); i++){
            resultado = resultado * calculadora.getNumeros().get(i);
        }
        return new RespostaDTO(resultado);
    }

    public RespostaDivisaoDTO dividir(Calculadora calculadora){
        Double resultado = calculadora.getNumeros().get(0).doubleValue();
        for(int i = 1; i < calculadora.getNumeros().size(); i++){
            if(resultado < calculadora.getNumeros().get(i)){
               throw new ArithmeticException("Não é possível dividir por um número maior");
            }
            resultado = resultado / calculadora.getNumeros().get(i);
        }
        return new RespostaDivisaoDTO(resultado);
    }
}
