package br.com.calculadora.controllers;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.DTOs.RespostaDivisaoDTO;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"São necessários ao menos 2 números para realizar a soma");
        }
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"São necessários ao menos 2 números para realizar a soma");
        }
        else if(!NumerosNaturais(calculadora)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Informe apenas números naturais");
        }
        else if(calculadora.getNumeros().get(0) < calculadora.getNumeros().get(1)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"O primeiro número deve ser maior que o segundo");
        }
        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"São necessários ao menos 2 números para realizar a multiplicação");
        }
        else if(!NumerosNaturais(calculadora)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Informe apenas números naturais");
        }
        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public RespostaDivisaoDTO dividir(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"São necessários ao menos 2 números para realizar a divisão");
        }
        else if(!NumerosNaturais(calculadora)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Informe apenas números naturais");
        }
        return calculadoraService.dividir(calculadora);
    }

    public boolean NumerosNaturais(Calculadora calculadora){
        boolean retorno = true;
        for(Integer numero : calculadora.getNumeros()){
            if(numero < 0){
                retorno = false;
            }
        }
        return retorno;
    }
}
