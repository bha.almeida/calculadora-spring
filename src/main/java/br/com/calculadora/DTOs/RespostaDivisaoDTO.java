package br.com.calculadora.DTOs;

public class RespostaDivisaoDTO {
    private Double resultado;

    public RespostaDivisaoDTO(Double resultado) {
        this.resultado = resultado;
    }

    public Double getResultado() {
        return resultado;
    }

    public void setResultado(Double resultado) {
        this.resultado = resultado;
    }
}
